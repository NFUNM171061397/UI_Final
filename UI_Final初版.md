# UI_Final

## A3海报一份

![海报](https://images.gitee.com/uploads/images/2020/0702/024744_0c270e82_1831468.jpeg "1.jpg")

## A.品牌核心价值
![核心价值](https://images.gitee.com/uploads/images/2020/0702/054310_643a5ca7_3043318.png "态 1.png")

## B.品牌元素

### 1. logo系统

![logo系统](https://images.gitee.com/uploads/images/2020/0702/004807_6ddc8cd9_1831468.png "logo系统.png")

### 2.颜色系统

![颜色系统](https://images.gitee.com/uploads/images/2020/0702/004832_ee14aff1_1831468.png "颜色系统.png")

### 3. ICON视觉系统
![icon](https://images.gitee.com/uploads/images/2020/0701/213621_9fa4be22_1831467.png "截屏2020-07-01 下午9.35.59.png")

### 4. 字体系统+尺寸大小规范及应用场景
![字体系统](https://images.gitee.com/uploads/images/2020/0702/054401_febaf4fa_3043318.png "主页_状态 1.png")

### 5. 图片使用规范
![图片规范](https://images.gitee.com/uploads/images/2020/0702/054434_ae42601c_3043318.png "页面 2_状态 1.png")

### 6. 版式、栏高、边距等尺寸规范+栅格系统说明

![尺寸规范](https://images.gitee.com/uploads/images/2020/0701/221820_20f2a719_1831467.png "截屏2020-07-01 下午10.09.58.png")

* 导航栏布局
![导航栏布局](https://images.gitee.com/uploads/images/2020/0701/220829_fe4a09de_1831467.png "截屏2020-07-01 下午10.07.35.png")

* 栅格系统说明
![栅格系统](https://images.gitee.com/uploads/images/2020/0702/002433_28a98fde_1831467.png "截屏2020-07-02 上午12.22.21.png")

## B.高保真原型图
![高保真1](https://images.gitee.com/uploads/images/2020/0702/032805_ef5f153b_3043318.png "a1.PNG")

![高爆真2](https://images.gitee.com/uploads/images/2020/0702/032850_22608a8d_3043318.png "a2.PNG")

![高保真3](https://images.gitee.com/uploads/images/2020/0702/033055_37f44985_3043318.png "a4.PNG")

[高保真原型图](http://nfunm171061397.gitee.io/ui_final)
