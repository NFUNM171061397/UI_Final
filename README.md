# UI_Final

# 🖤旅迹
## [一定要打开的原型](https://modao.cc/app/058d62d621f112188043fca6b97a76f7edefd4b3?simulator_type=device&sticky)

### :checkered_flag: 周边打卡➕地标打卡，一款适合年轻人的轻社交软件，更多功能等你发现。

## A3海报
作品介绍：旅迹APP是一款轻社交的照片打卡平台，周边打卡➕地标打卡，年轻人一起互动交流探索更多未知。

![海报](https://images.gitee.com/uploads/images/2020/0814/103949_c96b1fb4_1831468.jpeg "海报.jpg")

功能介绍：
* 地标打卡：通过打卡的方式来记录自己的旅行轨迹，世界之大如此美好，一起旅游吧～
* 周边打卡：发现周边好去处，打卡周边热门地点，一起探索网红店吧～
* 互动交流：年轻人一起互动交流，分享照片与作品，探索世界更多未知！



## A.品牌核心价值
![品牌核心价值](https://images.gitee.com/uploads/images/2020/0708/112157_b2c6dbc1_1831467.png "品牌核心价值 (1).png")

## B.品牌元素

### 1. logo系统

![logo](https://images.gitee.com/uploads/images/2020/0709/040332_f16b1dc4_1831468.png "logo_状态 1.png")

### 2.颜色系统

![color](https://images.gitee.com/uploads/images/2020/0709/041048_321f59e1_1831468.png "color.png")

### 3. ICON视觉系统

![icon](https://images.gitee.com/uploads/images/2020/0709/042021_c5810ff4_1831468.png "icon.png")

### 4. 字体系统+尺寸大小规范及应用场景
![文字规范](https://images.gitee.com/uploads/images/2020/0709/021054_2d50ecbd_3043318.png "状态 1.png")
### 5. 图片使用规范
![图片规范](https://images.gitee.com/uploads/images/2020/0709/021129_a2df2911_3043318.png "页面态 1.png")
### 6. 版式、栏高、边距等尺寸规范+栅格系统说明
![版式](https://images.gitee.com/uploads/images/2020/0709/030542_ab72727a_3043318.png "3_状态 1.png")


* 导航栏布局

![布局](https://images.gitee.com/uploads/images/2020/0709/041855_e1efb9e3_3043318.png "捕p.PNG")

* 栅格系统说明

![栅格](https://images.gitee.com/uploads/images/2020/0709/054430_79fb08f8_3043318.png "页面 5_状态 1.png")

## B.高保真原型图

##  :checkered_flag: [一定要打开的原型](https://modao.cc/app/058d62d621f112188043fca6b97a76f7edefd4b3?simulator_type=device&sticky)


![高保真原型图1](https://images.gitee.com/uploads/images/2020/0709/053251_f5c19176_3043318.png "高保真 1.png")

![高保真原型图2](https://images.gitee.com/uploads/images/2020/0709/053326_075fd5e6_3043318.png "高爆真2.png")

![高保真原型图3](https://images.gitee.com/uploads/images/2020/0709/053407_38747f2a_3043318.png "高保真4.png")


